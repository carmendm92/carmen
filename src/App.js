import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Sidebar from './components/Sidebar.jsx'
import MainDash from './components/MainDash/MainDash';


function App() {
  return (
    <div className="App">
      <div className='AppGlass'>
        <Sidebar />
        <MainDash />
      </div>
    </div>
  );
}

export default App;

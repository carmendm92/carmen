import React, { useState } from 'react';
import './Sidebar.css';
import { UilBars } from '@iconscout/react-unicons';
// import { Link } from 'react-scroll';

import { SidebarData } from './Data';

import { motion } from 'framer-motion';

const Sidebar = () => {

    const [selected, setSelected] = useState(0);
    const [expanded, setExpanded] = useState(true);

    const sidebarVariants = {
        true: {
            left: '0'
        },
        false: {
            left: '-60%'
        }
    }

    return (
        <>
            <div className='bars' style={expanded ? { left: '60%' } : { left: '5%' }}
                onClick={() => setExpanded(!expanded)}>
                <UilBars />
            </div>
            <motion.div className='Sidebar'
                variants={sidebarVariants}
                animate={window.innerWidth <= 768 ? `${expanded}` : ''}
            >
                {/* LOGO */}
                <div className='logo'>
                    <span>
                        Menu
                    </span>
                </div>
                {/* MENU */}
                <div className='menu'>
                    {SidebarData.map((item, index) => {
                        return (
                            <div
                                className={selected === index ? "menuItem active" : "menuItem"}
                                key={index}
                                onClick={() => setSelected(index)}
                            >
                                <item.icon />
                                <a className="nav-link" href={'#' + item.name}>
                                    <span>{item.heading}</span>
                                </a>
                            </div>

                        );
                    })}
                </div>
            </motion.div>
        </>
    )

}

export default Sidebar

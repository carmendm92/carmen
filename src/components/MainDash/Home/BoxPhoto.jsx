import React from 'react'
import './BoxPhoto.css'
function BoxPhoto() {

    // const boxes = document.querySelectorAll(".box");
    // var allBoxesLenght = boxes.length;

    // var indexImage = 0;
    // var init = function () {
    //     imageInterval();
    //     initBoxesShow();
    // };

    // init();

    // var images = [
    //     "jump",
    //     "lidl",
    //     "lidl_mascherina",
    //     "parapendio",
    //     "elegante",
    //     "vino",
    //     "pc"
    // ];

    // function imageInterval() {
    //     setInterval(() => {
    //         let min = 0;
    //         let max = 6;
    //         indexImage = Math.floor(Math.random() * (max - min + 1) + min);
    //     }, 1000);
    // }

    // function initBoxesShow() {
    //     var i = 0;
    //     setInterval(() => {
    //         images.forEach(function (image) {
    //             boxes[i].classList.forEach(function (classImage) {
    //                 if (image === classImage) {
    //                     boxes[i].classList.remove(image);
    //                     boxes[i].classList.add(images[indexImage]);
    //                 }
    //             })
    //         })
    //         if (i < allBoxesLenght - 1) {
    //             i++;
    //         } else {
    //             i = 0
    //         }
    //     }, 1000);
    // }
    return (<>
        <div className="col-4 img">
            <div className="box elegante"></div>
        </div>

        <div className="col-4 img">
            <div className="box lidl_mascherina"></div>
        </div>

        <div className="col-4 img">
            <div className="box vino"></div>
        </div>

        <div className="col-4 img">
            <div className="box jump"></div>
        </div>

        <div className="col-4 img">
            <div className="box parapendio"></div>
        </div>
    </>
    )
}

export default BoxPhoto

import './Home.css';
import BoxPhoto from './BoxPhoto';

function Home() {
    return (
        <>
            <div id="Home" className='ps-3 mt-3'>
                <header className="header_my_page"></header>
                <div className="container">
                    <h1 className="title ms-5 mt-3"> Carmen Di Mauro</h1>
                </div>
                <main className="mt-5">
                    <h2 className="text-center fw-bold m-5">Chi sono</h2>
                    <div className="row justify-content-center align-items-center">
                        <div className="col-12 col-md-4">
                            <p className="p-1 text-justify">
                                Amante della <span className="fw-bold">tecnologia</span> e delle sfide, mi considero una <span
                                    className="fw-bold">creativa</span>.
                                Nella vita vorrei creare, dare vita a qualcosa di unico, esprimere ciò che penso attraverso qualcosa che si possa toccare con mano.
                                Il mio posto preferito nel mondo è ovunque ci sia il mare.
                            </p>
                        </div>

                        <div className="col-12 col-md-7">
                            <div className="row justify-content-center">
                                <BoxPhoto />
                            </div>
                        </div>
                    </div>
                </main>

            </div>
        </>
    )
}

export default Home

import './Education.css';
import aulab from '../../../img/aulab.png';

function Education() {
    return (
        <>
            <div id="Education" className="ps-3 mt-3">
                <h2 className="text-center fw-bold m-5">Istruzione e formazione</h2>

                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-12 d-flex justify-content-center align-items-center">
                            <div className="square text-center"></div>
                        </div>

                        <div className="text-end p-2 col-6 col-md-4 margin-r">
                            <h3>2021</h3>
                            <a className="text-dark text-decoration-none" href="https://aulab.it/">
                                <h5 className="fw-bold text-end">Aulab</h5>
                            </a>
                            <p className="fw-bold text-end">Web Developer Full Stack Junior (400 ore)</p>
                            <p>Corso intensivo online sullo sviluppo web, aromenti trattati: <br /> HTML5, CSS3, Bootstrap5
                                <br /> GIT <br /> Console UNIX <br /> Visual Studio <br /> Javascript, Vue.JS. <br />
                                PHP 8.0, Laravel 8.0, Database My Sql <br /> SEO <br /> SCRUM, metodologia agile,
                                extreme programming <br /> Laravel Nova <br /> Forge, Horizon
                            </p>
                        </div>

                        <div className="col-6 col-md-4 mt-5 px-3">
                            <a className="text-dark text-decoration-none" href="https://aulab.it/"><img src={aulab}
                                className="img-fluid" alt="attestato aulab" /></a>
                        </div>

                        <div className="col-12 d-flex justify-content-center align-items-center">
                            <div className="square text-center"></div>
                        </div>

                        <div className="col-6 col-md-4 margin-r"></div>
                        <div className="p-2 col-6 col-md-4">
                            <div>
                                <h3>2013</h3>
                                <h5 className="fw-bold">I.A.L. CISL Sicilia - Ente per l'Orientamento e la Formazione Professionale</h5>
                                <p className="fw-bold">Addetto ai servizi per l'infanzia (900 ore) </p>
                                <p>Corso di formazione professionale, fondamentale per la mia crescita personale, gli argomenti che sono
                                    stati utili per la mia formazione
                                    sono stati:
                                    <ul>
                                        <li>Cenni di Primo Intervento</li>
                                        <li>Gestione e Organizzazione d’Impresa</li>
                                        <li>Elementi di Legislazione Sociale e Sanitaria</li>
                                    </ul>
                                </p>
                            </div>
                        </div>

                        <div className="col-12 d-flex justify-content-center align-items-center">
                            <div className="square text-center"></div>
                        </div>

                        <div className=" text-end p-2 col-6 col-md-4 margin-r">
                            <h3>2011</h3>
                            <h5 className="fw-bold">Maturità</h5>
                            <p className="fw-bold text-end">Liceo Socio-Psico-Pedagogico</p>
                            <p>Voto 80/100, materie studiate: lingua straniera (Inglese) <br /> latino <br /> statistica, informatica
                                <br /> storia dell'arte <br /> psicologia, sociologia, filosofia, pedagogia
                            </p>
                        </div>
                        <div className="col-6 col-md-4 px-3"></div>

                        <div className="col-12 d-flex justify-content-center align-items-center">
                            <div className="square text-center"></div>
                        </div>

                        <div className="col-6 col-md-4 margin-r"></div>
                        <div className="p-2 col-6 col-md-4">
                            <div>
                                <h3>2009</h3>
                                <h5 className="fw-bold">AICA</h5>
                                <p className="fw-bold">ECDL (European Computer Driving Licence) full Standard</p>
                                <p>Primo approccio con l'informatica, moduli trattati:
                                    <ul>
                                        <li>Concetti di base della tecnologia della informazione tecnologica </li>
                                        <li>Uso del computer e gestione dei file </li>
                                        <li>Elaborazione testi</li>
                                        <li>Foglio elettronico </li>
                                        <li>Database</li>
                                        <li>Presentazione</li>
                                        <li>Reti informatiche- Internet</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </>

    )
}

export default Education

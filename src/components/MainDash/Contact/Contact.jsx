import { UilInstagram } from "@iconscout/react-unicons";
import { UilGitlab } from "@iconscout/react-unicons";
import { UilLinkedin } from "@iconscout/react-unicons";
import { GrPinterest } from 'react-icons/gr';


function Contact() {
    return (
        <>
            <div id="Contact" className="social container mt-5 ps-3">
                <h2 className="text-center fw-bold">Social e Contact</h2>
                <div className="row ">
                    <div className="text-center my-5">
                        <a href="https://www.linkedin.com/in/carmen-di-mauro/" className="btn text-info">
                            <UilLinkedin />
                        </a>
                        <a href="https://gitlab.com/carmendm92" className="btn text-white">
                            <UilGitlab />
                        </a>
                        <a href="https://www.instagram.com/c.a.r.m.e.n.9.2/" className="btn text-warning">
                            <UilInstagram />
                        </a>
                        <a href="https://www.pinterest.it/carmendm92/" className="fs-5 btn text-danger">
                            <GrPinterest />
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Contact

import React from 'react';
import Home from '../MainDash/Home/Home';
import Education from './Education/Education';
import Work from './Work/Work';
import Contact from './Contact/Contact';
import './MainDash.css'

const MainDash = () => {
    return (
        <div className='MainDash'>
            <Home />
            <Education />
            <Work />
            <Contact />
        </div>
    )
}

export default MainDash

import blogDisegno from './../../../img/blog_disegno.png';
import dentista from './../../../img/dentista.png';
import creativityFood from './../../../img/creativity_food.png';
import presto from './../../../img/presto.png';


import bootstrap from './../../../img/BootStrap.png';
import digitalOcean from './../../../img/digitalocean.png';
import dropzone from './../../../img/dropzone.png';
import laravel from './../../../img/laravel.png';
import mySql from './../../../img/mysql.png';
import php from './../../../img/php.png';
import spatie from './../../../img/spatie.png';
import forge from './../../../img/forge.png'
const data = {
    cardData: [
        {
            id: 1,
            title: 'Creativity Food',
            img: creativityFood,
            link: 'https://gitlab.com/carmendm92/c-food',
            desc: [
                'Ottimizzato per la SEO con punteggio di 92/100',
                'Utilizzo del markup dei dati strutturati sulle pagine Web',
                'Responsive',
                'Widget per i social',
                'Logica di autenticazione con la libreria Fortify',
                'Gestione CRUD delle risorse accessibile solo all’admin, creando un middlewere e un comando personalizzato',
                'Rotte parametriche',
                'Ricerca full-text',
                'Resize delle immagini'
            ],
            logo: [
                forge,
                digitalOcean,
                php,
                bootstrap,
                dropzone,
                laravel,
                mySql
            ]
        },
        {
            id: 2,
            title: 'Presto.it',
            img: presto,
            link: 'https://gitlab.com/carmendm92/presto.it_2',
            desc: [
                'Rotte parametriche',
                'Diverse logiche quali guest, user e revisor',
                'Multilingue',
                'Responsive',
                'Resize delle immagini',
                'Applicazione watermark',
                'API GoogleVision per l’applicazione di sticker per nascondere i volti, etichette e allert per i contenuti sensibili',
                'Ricerca full-text',
                'Utilizzo delle code attraverso l\' utilizzo dei Job'
            ],
            logo: [
                php,
                laravel,
                bootstrap,
                dropzone,
                spatie,
                mySql,
            ]
        },
        {
            id: 3,
            title: 'Blog Disegno',
            img: blogDisegno,
            link: 'https://gitlab.com/carmendm92/blog_disegno',
            desc: [
                'Rotte parametriche',
                'Design pattern MVC',
                'Building degli asset attraverso NPM',
                'Responsive',
                'Form di contatto funzionante',
            ],
            logo: [
                php,
                laravel,
                bootstrap,
                mySql,
            ]
        },
        {
            id: 4,
            title: 'Il tuo dentista',
            img: dentista,
            link: 'https://gitlab.com/carmendm92/il_tuo_dentista',
            desc: [
                'Rotte parametriche',
                'Autenticazione con la libreria Fortify',
                'Responsive',
                'Diverse logiche quali guest e user',
                'Design pattern MVC',
            ],
            logo: [
                php,
                laravel,
                bootstrap,
            ]
        }
    ],
};
export default data;
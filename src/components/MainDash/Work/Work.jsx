import React, { useState } from 'react';
import './Work.css';
import data from './Data';
import Modal from './Modal'


const Work = () => {
    const [modal, setModal] = useState(false);
    const [tempdata, setTempdata] = useState([]);

    const getData = (img, title, link, desc, logo) => {
        let tempData = [img, title, link, desc, logo];
        setTempdata(item => [1, ...tempData]);
        return setModal(true);
    }
    return (
        <>
            <div id="Work" className="ps-3 mt-5 container text-center" >
                <h2 className="mb-5 fw-bold">Progetti</h2>
                <div className="row justify-content-center">
                    {data.cardData.map((item, index) => {
                        return (
                            <div className="col-12 col-md-5 p-3 box-work" key={index}>
                                <h4 className="fw-bold">{item.title}</h4>
                                <a href={item.link} className="btn text-white">
                                    <span>Codice </span><i className="fa-brands fa-gitlab"></i> Gitlab
                                </a>
                                <div className="box">
                                    <button className="btn" onClick={() => getData(item.img, item.title, item.link, item.desc, item.logo)}>
                                        <img src={item.img} className="img-fluid border border-dark"
                                            alt="anteprima sito creativity food" data-bs-toggle="modal" data-bs-target="#video_creativity" />
                                    </button>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
            {
                modal === true ? <Modal img={tempdata[1]} title={tempdata[2]} link={tempdata[3]} desc={tempdata[4]} logo={tempdata[5]} hide={() => setModal(false)} /> : ''
            }
        </>
    )
}
export default Work;
import './Modal.css'
import React, { Component } from 'react';

export default class Modal extends Component {
    render() {
        return (
            <div className="modal show fade ciao">
                <div className="modal-dialog modal-dialog-centered modal-lg">
                    <div className="modal-content m-0 p-0">
                        <div className="modal-header">
                            <h5 className="modal-title text-dark fw-bold">{this.props.title}</h5>
                            <button type="button" className="btn" onClick={this.props.hide}>
                                <span className="material-symbols-outlined">close</span>
                            </button>
                        </div>
                        <div className="modal-body d-flex justify-content-center">
                            <div className="container">
                                <div className="row">
                                    <div className="col-12">
                                        <img src={this.props.img} className="img-fluid" alt="anteprima" />
                                        {/* <iframe width="100%" height="400" src="https://www.youtube.com/embed/VjZz3iQK7UU?html5=1"
                                            title=" YouTube video player" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe> */}
                                    </div>
                                    <div className="col-12 text-white ">
                                        <h4 className="text-center my-5">Strumenti & Skills</h4>
                                        <ul>
                                            {this.props.desc.map((desc) => {
                                                return (
                                                    <li key={desc.id}>{desc}</li>
                                                )
                                            })}
                                        </ul>
                                    </div>

                                    <div className='text-center'>
                                        {this.props.logo.map((logo, index) => {
                                            return (
                                                <img key={index} width="120px" src={logo} alt="logo" className='p-2 outline' />
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

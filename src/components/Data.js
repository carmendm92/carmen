import {
    UilEstate,
    UilGraduationCap,
    UilFolder,
    UilUsersAlt,
} from '@iconscout/react-unicons';

export const SidebarData = [
    {
        icon: UilEstate,
        heading: "Home",
        name: 'Home'
    },
    {
        icon: UilGraduationCap,
        heading: "Education",
        name: 'Education'

    },
    {
        icon: UilFolder,
        heading: "Work",
        name: 'Work'

    },
    {
        icon: UilUsersAlt,
        heading: "Contact",
        name: 'Contact'

    },

];

